##Authors
Steven Doolan
Shane Bruggeman
Austin Niccum
Davis Nygren

##Description
Chop chop is a collaborative effort to increase nonprofit funding efforts on hot-button issues by manipulating the divisive world we vote in. Simply put, Chop Chop is competitive charity.

https://docs.google.com/document/d/1jBIxeVYqLvF0QEL4qvGDwHUHx4PhLqLtbu4y3tYOcy8/edit#heading=h.ofwyf2dwfyhf

##Developer Setup
````bash
npm install     # Install all package dependencies
npm start       # Launch the application on http://localhost:3000
````

###npm install errors
In the unlikely case Semanitic UI has installation errors:
```bash
npm install -g gulp
npm install semantic-u --save 
#Or
npm install
#Then
cd semantic-ui/
gulp build
```
##Current Sprint Three Status
###Voting
Voting allows the user to identify the two conflicting sides and vote for one or both. The user can hover over the logo to read more about the side and click the link to visit the corrosponding site.

A line chart in the background depicts the voting status.
![alt text](./documentation/s2-voting-page.PNG "Voting Page")
###Forum
The forum allows users to engage in discussion and interact with each other. The forum page was redefined to match updated client specs. The current format allows for tree themed orginization. Users can click on a post to "highlight" the post. This will also reveal its replies. The user can then click on any of the replies an traverse down the reply to reply tree path. The buttons at the bottom of posts update the Grow (like) count, the Burn (dislike) count, and allow the user to reply.

![alt text](./documentation/s3-forum.png "Forum Page")
##### Reply to Post
The forum page now allows the user to reply to a post by clicking the comment bubble on a post.
![alt text](./documentation/s3-forum-create-reply.png "Forum Reply")
##### Create new Post
The forum page now allows the user to create a new post by clicking create a new post.
![alt text](./documentation/s3-forum-create-post.png "Forum New Post")

###Account
The account section allows users to view and modify specific account information.
![alt text](./documentation/s2-account-page.PNG "Account Page")

###Admin Page
The admin page allows the system administrator to update, stage, and replace campaigns.
![alt text](./documentation/s3-admin.png "Admin Page")
![alt text](./documentation/edit-admin-2.png "Admin Page")
![alt text](./documentation/edit-admin-1.png "Admin Page")

###Backend 
Initial Database setup 
Attached Passport to handle auth
Unfortunately, at the close of the project we were unable to provide reliable backend services to the application. Consequently our frontend relies upon data supplied via services.

### Testing
The proposed development plan agreed upon between team, client, and instructor did not include plans or requirements for testing. Testing was never discussed nor was feedback ever received pertaining to it. Therefore the team sought to focus on implementing features in the small time available to them rather than to fully test them.