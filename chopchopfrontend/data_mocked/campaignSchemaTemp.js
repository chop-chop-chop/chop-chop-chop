/**
 * Created by Steven on 2/17/2016.
 */
var mongoose = require("mongoose");

mongoose.models = {};
mongoose.modelSchemas = {};

var voteSchema = new mongoose.Schema();
vote.add({
    _id:{
        type:String,
        index: {unique: true}
    },
    date: Date,
    count:number
});


var factionSchema = new mongoose.Schema();
factionSchema.add({
    _id:{
        type:String,
        index: {unique: true}
    },
    name: String,
    imagePath: String,
    description: String,
    link: String,
    votes:[voteSchema]

});


var campaignSchema = new mongoose.Schema();
campaignSchema.add({
    _id:{
        type:String,
        index: {unique: true}
    },
    campaignName: String,
    leftFaction: factionSchema,//keeps track of side better and cleaner
    rightFaction: factionSchema,
    orderNumber: Number,//Next order = 0 if active, negitive if over, 1 if next campaign ....
    startDate: Date,//nullable
    endDate: Date//nullable
});


module.exports = mongoose.model('Vote', voteSchema);
module.exports = mongoose.model('Faction', factionSchema);
module.exports = mongoose.model('Campaign', campaignSchema);
