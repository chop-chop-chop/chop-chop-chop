(function() {
    var app = angular.module('ChopChopApp', ['ngRoute','DataManager', 'googlechart', 'ngFileUpload']);
    app.config(["$routeProvider", function($routeProvider, $routeParams) {
        $routeProvider
            .when('/home', {
                templateUrl: '../views/home.html'
                // no controller needed for home
            }).when('/admin', {
                templateUrl: '../views/admin/admin.html'
                // no controller needed for home
            })
            .when('/account', {
                templateUrl: '../views/account.html'
                // no controller needed for home
            })
            .otherwise({
                redirectTo: '/home'
            });
    }]);
    app.exports = app;
})();