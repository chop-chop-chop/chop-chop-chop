angular.module('ChopChopApp')
    .filter('shortenString', function () {
        return function (input, num) {
            if(!input || num == -1)
                return input;
            return input.substring(0, num);

        };
    })
    .filter('commentCount', function () {
        return function (post) {
            if(!post || !post.replies)
                return 0;
            return post.replies.length;
        };
    })
    .filter('arrayCount', function(){
        return function (input){
            console.log(input);
            if(!input)
                return 0;
            var count = 0;
            for(var i in input){
                count += input[i].count;
            }
            return count;
        }
    }).filter('doubleArrayCount', function(){
        return function(array1, array2){
            var count = 0;
            if(array1) {
                for (var i in array1) {
                    count += array1[i].count;
                }
            }
            if(array2) {
                for (var j in array2) {
                    count += array2[j].count;
                }
            }
            return count;
        };
});