angular.module('ChopChopApp')
    .controller('NavBarCtrl', function ($scope, $route, $location,$routeParams) {

        $('#main-nav-menu').sidr({
            name: 'nav',
            displace: false
        });

        $scope.accessLevel = {
            user: true,
            admin: true
        };

        $scope.userAccount ={
            username: '',
            password:'',
            name: '',
            email: ''
        };

        $scope.currentModal = {
              //login: true,
              signup:false
        };

        $scope.navigateToHomeElement = function(tag){
            $scope.closeSlider();
            if($route.current.$$route.originalPath !== '/home') {
                $location.path('/home/'+tag);
            }else {

                $('html, body').animate({
                    scrollTop: $(tag).offset().top
                }, 1000);
            }
        };

        $scope.navigateToNonHomePage= function (path) {
            console.log(path);
            $scope.closeSlider();
            $location.path(path);
        };

        $scope.hasAdminAccess = function(){
            return $scope.accessLevel.admin;
        };

        $scope.hasUserAccess = function(){
            return $scope.accessLevel.user;
        };

        $scope.closeSlider = function () {
            event.preventDefault();
            $.sidr('close', 'nav');
        };

        $scope.openSlider = function () {
            event.preventDefault();
            $.sidr('open', 'nav');
        };


        $scope.validateLogin = function () {
            if (!$('#loginModal .ui.form').form('validate form')) {
                $('#loginModal .form .message').show();
                return false;
            }
            return true;
        };

        $scope.launchLogin = function(){
            $scope.closeSlider();
            $('#loginModal')
                .modal('show')
                .modal('refresh')//re-centers
                .modal('attach events', '#loginModal .remove', 'hide')
                .modal({
                    closable: false,
                    onDeny: function () {
                        return true;
                    },
                    onApprove: $scope.validateLogin
                });

            $('#loginModal .form').form({
                fields: {
                    username: {
                        identifier: 'email',
                        rules: [
                            {
                                type: 'email',
                                prompt: 'Please enter a username'
                            }
                        ]
                    },
                    password: {
                        identifier: 'password',
                        rules: [
                            {
                                type:'regExp[/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$&*_]).{8,24}$/]',
                                prompt:'Please enter a secure password'
                            //},
                            //{
                            //    type: 'regExp[/^(?=.*[A-Z])$/]', //'regExp^[/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$&*_]).{8,24}$/]',
                            //    //src: http://stackoverflow.com/questions/5142103/regex-for-password-strength
                            //    prompt: 'Passwords must contain at least one upper case letter'
                            //},
                            //{
                            //    type: 'regExp[/^(?=.*[a-z])$/]', //'regExp^[/(?=.*)(?=.*[0-9])(?=.*[!@#$&*_]).{8,24}$/]',
                            //    prompt: 'Passwords must contain at least one lower case letter'
                            //},
                            //{
                            //    type: 'regExp[/^(?=.*[0-9])$/]', //'regExp^[/(?=.*)(?=.*[0-9])(?=.*[!@#$&*_]).{8,24}$/]',
                            //    prompt: 'Passwords must contain at least one number'
                            //},
                            //{
                            //    type: 'regExp[/^(?=.*[!@#$&*_])$/]', //'regExp^[/(?=.*)(?=.*[0-9])(?=.*[!@#$&*_]).{8,24}$/]',
                            //    prompt: 'Passwords must contain at least special character'
                            //},
                            //{
                            //    type: 'minLength[8]', //'regExp^[/(?=.*)(?=.*[0-9])(?=.*[!@#$&*_]).{8,24}$/]',
                            //    prompt: 'Passwords must be at least 8 characters long'
                            }
                        ]
                    }
                }
            });
        };
    })

    .controller('ForumController', function ($scope, ForumManager) {
        $scope.colors = ['#FFE6E6', '#FFF1E6', '#E4FDE4',
            '#EFFDD2', '#F9CEE7', '#C9F3F3', '#EFFDD2'
        ];

        $scope.selectedPost = null;

        $scope.posts = {};

        $scope.headers = [];

        ForumManager.query(function () {
        });

        ForumManager.resetModal = function () {
            $scope.modalPost = {
                newPost: {
                    title: '',
                    category: '',
                    body: ''
                },
                parentPost: null
            };
        };

        ForumManager.getDataAndHeaders(function (data, headers) {
            $scope.posts = data;
            $scope.headers = headers;

            $('#forum .masthead')
                .sticky({
                    context: '#forum'
                });

        });

        $scope.growButtonClick = function (post) {
            if (post.watered)
                return false;
            post.grows++;
            post.watered = true;
            if (post.burned) {
                post.burned = false;
                post.burns--;
            }
        };

        $scope.burnButtonClick = function (post) {
            if (post.burned)
                return false;
            post.burns++;
            post.burned = true;
            if (post.watered) {
                post.watered = false;
                post.grows--;
            }
        };

        $scope.postClick = function (post) {
            //set or remove the big post
            $scope.selectedPost = $scope.selectedPost != post ? $scope.selectedPost = post : null;
        };

        $scope.launchNewPostModal = function (parentPost) {
            ForumManager.resetModal();
            $scope.modalPost.parentPost = parentPost;
            $('#forumModal button.post-reply').addClass('disabled');
            $('#forumModal')
                .modal({
                    closable: false,
                    onDeny: function () {
                        return true;
                    },
                    onApprove: $scope.createNewPost
                })
                .modal('show')
                .modal('refresh')//re-centers
                .modal('attach events', '#forumModal .remove', 'hide');
                $('#forumModal .form').form('clear errors');
                $('#forumModal .form .message').hide();
            if(parentPost) {
                $('#forumModal .form').form({
                    fields: {
                        body: {
                            identifier: 'body',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please add a body'
                                }
                            ]
                        }
                    }
                });
            }
            else{
                $('#forumModal .form').form({
                    fields: {
                        title: {
                            identifier: 'title',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please add a title'
                                }
                            ]
                        },
                        category: {
                            identifier: 'category',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please select a category'
                                }
                            ]
                        },
                        body: {
                            identifier: 'body',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please add a body'
                                }
                            ]
                        }
                    }
                });
            }
        };

        $scope.createNewPost = function () {
            if (!$('#forumModal .ui.form').form('validate form')) {
                $('#forumModal .form .message').show();
                return false;
            }

            var parent = $scope.modalPost.parentPost;
            var newPost = $scope.modalPost.newPost;
            if (parent) {
                $scope.$apply(
                    parent.replies.push({
                        id: -1,
                        title: parent.title,
                        body: newPost.body,
                        userId: 0,
                        userName: "",
                        "grows": 0,
                        "burns": 0,
                        "replies": []

                    })
                );
            }
            else {
                $scope.$apply(
                    $scope.posts[newPost.category].push(
                        {
                            "id": "-1",
                            "title": newPost.title,
                            "body": newPost.body,
                            "userId": 0,
                            "userName": "",
                            "grows": 0,
                            "burns": 0,
                            "replies": [],
                            "category": newPost.category
                        }));
            }
            ForumManager.resetModal();
            return true;
        };
    })
    .controller('VotingCtrl', ['$scope', 'AdminCampaignService','CampaignService', 'ChartService', function($scope,AdminCampaignService, CampaignService, ChartService) {


        // manually keep track of what day it is
        //$scope.dateIndicator = 'Sunday';

        var base = 400;

        var init = function() {
            // model for a card
            var defaultCard = {
                name: 'DefaultCard',
                imagePath: 'img/logo.png',
                description: 'Please wait for other cards to load!',
                link: 'http://patienceisavirtue.com/'
            };

            $scope.leftCard = {};
            $scope.rightCard = {};

            // cards are initially defaulted
            angular.copy(defaultCard, $scope.leftCard);
            angular.copy(defaultCard, $scope.rightCard);
        };
        init();

        AdminCampaignService.getCurrentCampaign(function(data){
           $scope.leftCard = data.leftFaction;
           $scope.rightCard = data.rightFaction;
        });

        var campaignName = 'The Role of Guns in America';
        var factionLeftName = 'The National Rifle Association';
        var factionRightName = 'Coalition to End Gun Violence';

        //function initLeftCard() {
        //    var leftData = $scope.leftCardData;
        //    var keys = Object.keys(leftData);
        //
        //    // copy relevant campaign data to the cards
        //    for(var i = 0; i < keys.length; i++) {
        //        var key = keys[i];
        //        $scope.leftCard[key] = leftData[key];
        //    }
        //}
        //
        //function initRightCard() {
        //    var rightData = $scope.rightCardData;
        //    var keys = Object.keys(rightData);
        //
        //    // copy relevant campaign data to the cards
        //    for(var i = 0; i < keys.length; i++) {
        //        var key = keys[i];
        //        $scope.rightCard[key] = rightData[key];
        //    }
        //}


        $scope.closeModal = function() {
            $('.ui.modal').modal('hide');
        };

        $scope.openSharePopup = function(shareUrl, width, height) {
            $scope.closeModal();

            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            screenWidth = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            screenHeight = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((screenWidth / 2) - (width / 2)) + dualScreenLeft;
            var top = ((screenHeight / 2) - (height / 2)) + dualScreenTop;
            var popup = window.open(shareUrl, "pop", "width="+width+", height="+height+", top="+top+", left = "+left+", menubar=0, toolbar=0, scrollbars=no, location=0, status=0, resizable=no");
        };

        $scope.sharePopupFacebook = function() {
            this.openSharePopup("https://www.facebook.com/sharer/sharer.php?u=http://thechopchopsite.com", 600, 400);
            // var fbpopup = window.open("https://www.facebook.com/sharer/sharer.php?u=http://thechopchopsite.com", "pop", "width=600, height=400, scrollbars=no");
            return false;
        };

        $scope.sharePopupTwitter = function() {
            this.openSharePopup("https://twitter.com/intent/tweet?url=http://thechopchopsite.com&text=Check out &hashtags=chopchop", 800, 600);
            return false;
        };

        /**
         * Explanation: Define the voting counts on the $scope,
         * but update them according to the value in the voting service.
         *
         * Defining a get on this property but not a set allows us
         * to read the value simply (ie $scope.voteCountLeft) but not to
         * directly write a value to it (ie $scope.voteCountLeft = 10)
         *
         * To change its value, you must use the methods defined on the voting service
         */
        Object.defineProperty($scope, 'leftCardData', {
            get: function() {
                var data = CampaignService.getLeftFactionData(campaignName);
                return data;
            }
        });

        Object.defineProperty($scope, 'rightCardData', {
            get: function() {
                return CampaignService.getRightFactionData(campaignName);
            }
        });

        Object.defineProperty($scope, 'leftVotes', {
            get: function() {
                return CampaignService.getVotes(campaignName, factionLeftName);
            }
        });

        // retrieves the breakdown of votes by day for the right
        Object.defineProperty($scope, 'rightVotes', {
            get: function() {
                return CampaignService.getVotes(campaignName, factionRightName);
            }
        });

        // retrieves the value associated with today for the left
        Object.defineProperty($scope, 'todayLeftVal', {
            get: function() {
                return CampaignService.getScore(campaignName, factionLeftName, $scope.dateIndicator);
            }
        });

        // retrieves the value associated with today for the right
        Object.defineProperty($scope, 'todayRightVal', {
            get: function() {
                return CampaignService.getScore(campaignName, factionRightName, $scope.dateIndicator);
            }
        });

        //// vote for the left side
        //$scope.voteLeft = function() {
        //    //CampaignService.vote(campaignName, factionLeftName, $scope.dateIndicator, balanceBars);
			//$('#shareModal').modal('show');
        //};
        //
        //// vote for the right side
        //$scope.voteRight = function() {
        //    //CampaignService.vote(campaignName, factionRightName, $scope.dateIndicator, balanceBars);
        //    $('#shareModal').modal('show');
        //};

        $scope.vote = function(voteArray){
            //$scope.alreadyVoted = true;
            console.log(voteArray);
            $('#shareModal').modal('show');
            var tempDate = new Date();
            var dateString = ""+ tempDate.getMonth()+"/" + tempDate.getDate() +"/" + tempDate.getFullYear();
            for(var i = 0; i<voteArray.length; i++) {
                if (voteArray[i].date === dateString
                    && voteArray[i].date === dateString) {
                    //console.log("update vote",voteArr )
                    voteArray[i].count += 1;
                    return;
                }
            }
                voteArray.push({
                    id:-1,
                    date: dateString,
                    count:1
                });
            console.log(voteArray);
            $scope.$apply();
        };

        // compute the widths of the voting bars, so that each is proportionate
        // to the max base
        function balanceBars() {
            var leftBar = $('.left-bar'),
                rightBar = $('.right-bar');

            var lrsum = $scope.todayLeftVal + $scope.todayRightVal,
                leftPercent = $scope.todayLeftVal / lrsum,
                rightPercent = $scope.todayRightVal / lrsum;

            var leftSet = leftPercent * base,
                rightSet = rightPercent * base;

            leftBar.width(leftSet);
            rightBar.width(rightSet);
        }

        // do all the necessary startup tasks for the controller here
        function initVotingController() {
          //  balanceBars();
            //initLeftCard();
            //initRightCard();
        }

        //initVotingController();
    }])
    .controller('VotingLineChartCtrl', ['$scope','AdminCampaignService', 'CampaignService', 'ChartService', function($scope,AdminCampaignService, CampaignService, ChartService) {
        var campaignName = 'The Role of Guns in America';
        var factionLeftName = 'The National Rifle Association';
        var factionRightName = 'Coalition to End Gun Violence';


        $scope.leftFactionVotes=[];
        $scope.rightFactionVotes=[];

        AdminCampaignService.getCurrentCampaign(function(data){
            $scope.leftFactionVotes = data.leftFaction.votes;
            $scope.rightFactionVotes = data.rightFaction.votes;
        });
        Object.defineProperty($scope, 'leftVotes', {
            get: function() {
                return CampaignService.getVotes(campaignName, factionLeftName);
            }
        });

        Object.defineProperty($scope, 'rightVotes', {
            get: function() {
                return CampaignService.getVotes(campaignName, factionRightName);
            }
        });

        // the true parameter indicates deep watching
        $scope.$watch('leftVotes', function(oldValue, newValue) {
            // google.visualization may not have been fully loaded yet (https://www.gstatic.com/charts/loader.js)
            if($scope.initialized && google.visualization) {
                drawChart();
            }
        }, true);

        // the true parameter indicates deep watching
        $scope.$watch('rightVotes', function(oldValue, newValue) {
            // google.visualization may not have been fully loaded yet (https://www.gstatic.com/charts/loader.js)
            if($scope.initialized && google.visualization) {
                drawChart();
            }
        }, true);

        function drawChart() {;
            var dateStrings = CampaignService.getDates(campaignName);
            var dataBuilder = [];

            dataBuilder.push(['Day', factionLeftName, factionRightName]);
            //TODO: STEVEN LOOK AT
            for(var i = 0; i < dateStrings.length; i++) {

                // grab the relevant data
                var label = dateStrings[i],
                    left = $scope.leftVotes[i] || 0,
                    right = $scope.rightVotes[i] || 0;

                dataBuilder.push([label, left, right]);
            }

            var data = google.visualization.arrayToDataTable(dataBuilder);

            var options = {
                title: 'The Role of Guns in America',
                backgroundColor: '#E4E4E4',
                legend: { position: 'bottom' },
                chartArea: {'width': '90%', 'height': '85%'}
            };

            var chart = new google.visualization.LineChart(document.getElementById('voting-line-chart'));

            chart.draw(data, options);
        }

        function initVotingLineChartCtrl() {
            $scope.initialized = true;
            google.charts.setOnLoadCallback(drawChart);
        }

        initVotingLineChartCtrl();
    }])
    .controller('VotingBarChartCtrl', ['$scope', 'CampaignService', function($scope, CampaignService) {
        var campaignName = 'The Role of Guns in America';
        var factionLeftName = 'The National Rifle Association';
        var factionRightName = 'Coalition to End Gun Violence';

        Object.defineProperty($scope, 'leftVotes', {
            get: function() {
                return CampaignService.getVotes(campaignName, factionLeftName);
            }
        });

        Object.defineProperty($scope, 'rightVotes', {
            get: function() {
                return CampaignService.getVotes(campaignName, factionRightName);
            }
        });

        // the true parameter indicates deep watching
        $scope.$watch('leftVotes', function(oldValue, newValue) {
            // google.visualization may not have been fully loaded yet (https://www.gstatic.com/charts/loader.js)
            if($scope.initialized && google.visualization) {
                drawChart();
            }
        }, true);

        // the true parameter indicates deep watching
        $scope.$watch('rightVotes', function(oldValue, newValue) {
            // google.visualization may not have been fully loaded yet (https://www.gstatic.com/charts/loader.js)
            if($scope.initialized && google.visualization) {
                drawChart();
            }
        }, true);

        function drawChart() {
            var dateStrings = CampaignService.getDates(campaignName);
            var dataBuilder = [];

            var totalLeft = 0,
                totalRight = 0;

            for(var i = 0; i < dateStrings.length; i++) {
                totalLeft += $scope.leftVotes[i];
                totalRight += $scope.rightVotes[i];
            }

            dataBuilder.push(['Campaign', 'Number of Votes', {role: 'style'}]);
            dataBuilder.push([factionLeftName, totalLeft, 'color: blue; opacity: 0.5']);
            dataBuilder.push([factionRightName, totalRight, 'color: red; opacity: 0.5']);

            var data = google.visualization.arrayToDataTable(dataBuilder);

            var options = {
                title: campaignName,
                bars: 'horizontal',
                legend: {position: 'none'},
                vAxis: {
                    format:'0 votes',
                    title: 'Faction Name',
                    direction: -1,
                    slantedText: true,
                    slantedTextAngle: 90
                },
                hAxis: {
                    format:'0 votes',
                    title: 'Number of Votes'
                },
                backgroundColor: '#E4E4E4'
            };

            // var data = google.visualization.arrayToDataTable([
            //     ['Element', 'Density', { role: 'style' }],
            //     ['Copper', 8.94, '#b87333'],            // RGB value
            //     ['Silver', 10.49, 'silver'],            // English color name
            //     ['Gold', 19.30, 'gold'],
            //     ['Platinum', 21.45, 'color: #e5e4e2' ], // CSS-style declaration
            // ]);

            // var options = {
            //     bar: { groupWidth: '90%' },
            // };

            console.log(google);

            // var chart = new google.charts.Bar(document.getElementById('voting-bar-chart'));
            var chart = new google.visualization.BarChart(document.getElementById('voting-bar-chart'));
            chart.draw(data, google.charts.Bar.convertOptions(options));
        }

        function initVotingBarChartCtrl() {
            $scope.initialized = true;
            google.charts.setOnLoadCallback(drawChart);
        }

        initVotingBarChartCtrl();
    }])
    .controller('AdminCtrl', ['$scope', 'Upload', 'AdminCampaignService', function($scope, Upload, AdminCampaignService) {
        //require('jquery-ui/draggable');

        $scope.nextCampagins = [];
        $scope.currentCampaign ={};
        $scope.previewCampaign = {};
        //admin services
        AdminCampaignService.getNextCampaigns(function (data) {
            $scope.currentCampaign = data;
            $scope.previewCampaign = data[0];
            console.log("front end admin", data);
        });

        AdminCampaignService.getCurrentCampaign(function(data){
           $scope.currentCampaign = data;
        });

        // html element init
        $('#scheduledEndDate').datepicker();


        $scope.createPlaceholderCard = function(name, color) {
            return {
                name: name,
                imagePath: '../../img/logo.png',
                description: 'We choose to go to the moon. We choose to go to the moon in this decade and do the other things, not because they are easy, but because they are hard',
                link: 'http://bit.ly/217xU4u',
                warScores:[],
                color:color
            };
        };

        $('#placeholder').click(function() {
            $('#right-upload').trigger('click');
        });

        //temp init replace with service call
        //$scope.previewCampaign = {
        //    rightCard: $scope.createPlaceholderCard("Starting Right Card", 'red'),
        //    leftCard: $scope.createPlaceholderCard("Starting Left card", 'blue')
        //};
        //$scope.nextCampagins = [];
        //$scope.nextCampagins.push($scope.previewCampaign);


        $scope.previewFile = function() {
            var preview = $('#placeholder')[0];
            var file    = $('#right-upload')[0].files[0];
            var reader  = new FileReader();

            reader.addEventListener("load", function () {
                preview.src = reader.result;
                $scope.persistImage(reader.result);
            }, false);

            if (file) {
                reader.readAsDataURL(file);
            }
        };

        //Persit Image // I don't know what this does, but the images for the current campaign should not be changed
        //$scope.persistImage = function(img) {
        //    campaignName = 'The Role of Guns in America';
        //    factionName = 'The National Rifle Association';
        //    CampaignService.setImagePath(campaignName, factionName, img, function(bool) {
        //        if(bool) {
        //            console.log('Successfully changed the campaign service image');
        //        } else {
        //            console.log('Failed to change campaign service image');
        //        }
        //    });
        //};
        $scope.emptyFunc = function(){

        }  ;
        //admin pop up stuff

        $scope.swapLeftRight = function(campaign){
            var tempFaction = campaign.leftFaction;
            campaign.leftFaction = campaign.rightFaction;
            campaign.rightFaction = tempFaction;
        };

        $scope.endCampaign = function(){
            AdminCampaignService.endCampaign(function(data) {
                //AdminCampaignService.getCurrentCampaign(function(data){
                console.log("end data 1", data);
                $scope.currentCampaign = data;
                //$scope.currentCampaign

            }, function(data) {
                //});
                //AdminCampaignService.getNextCampaigns(function(data){
                console.log("end data 2", data);
                $scope.nextCampagins = data;
                $scope.previewCampaign = data.length> 0 ? data[0]: null;
                //$scope.$apply($scope.previewCampaign);
                //});
                //$scope.$apply();
            });

        };


        $scope.modalSettings ={
            //orgParent:
            leftCard:{
                card : true,
                form: false,
                width : "47%"
            },
            rightCard:{
                card : true,
                form: false,
                width : "47%"
            }
        };
        //var top =
        //var left = $(this).position().left;
        //$(this).data('orgTop', top);//
        //$(this).data('orgLeft', left);


        $scope.launchEditCampaignModal = function (campaign) {
            $scope.rightCard ={};
            $scope.leftCard ={};
            if(!campaign) {
                var newCampagin = true;
                $scope.rightCard = $scope.createPlaceholderCard("Right card", "red");
                $scope.leftCard = $scope.createPlaceholderCard("Left card","blue");
            }
            else{
                //copy to break binding and allow user to save/cancel updates
                angular.copy(campaign.rightFaction, $scope.rightCard);
                angular.copy(campaign.leftFaction, $scope.leftCard);
            }
            sliderActivationFunction();
            //$scope.modalSettings.leftCard.card = true;
            //$scope.modalSettings.leftCard.form = false;
            //$scope.modalSettings.rightCard.card = true;
            //$scope.modalSettings.rightCard.form = false;
            //$scope.modalSettings.leftCard.width = "47%";
            //$scope.modalSettings.rightCard.width = "47%";
            //$('.divider').data('originalParent', $('.divider').parent())
            //$(".divider").dat
            //};
            //$(".divider").data("ui-draggable").originalPosition = {
            //    top: 0,
            //    left: 0
            //};

            //console.log($scope.leftCard, $scope.rightCard);

            //console.log('hit');
            //ForumManager.resetModal();
            //$scope.modalPost.parentPost = parentPost;
            //$('#adminModal button.post-reply').addClass('disabled');

            $('#adminModal')
                .modal({
                    closable: false,
                    onDeny: function () {
                        return true;
                    },
                    onApprove:// $scope.createNewPost
                    function(){
                        return modalCloseEvent($scope.leftCard, $scope.rightCard, campaign)
                    }
                })
                .modal('setting', { allowMultiple: false, detachable:false })
                .modal('show')
                .modal('refresh')//re-centers
                .modal('attach events', '#adminModal .remove', 'hide');
            //$('#forumModal .form').form('clear errors');
            //$('#forumModal .form .message').hide();
            //$('').form({
            //        fields: {
            //            body: {
            //                identifier: 'body',
            //                rules: [
            //                    {
            //                        type: 'empty',
            //                        prompt: 'Please add a body'
            //                    }
            //                ]
            //            }
            //        }
            //    });
        };

        var modalCloseEvent = function(leftCard, rightCard, campaign){
            if(campaign){
                campaign.rightFaction = rightCard;
                campaign.leftFaction = leftCard;
                AdminCampaignService.updateCampaign(campaign);
                $scope.$apply(campaign);
                $scope.$apply($scope.previewCampaign);
                return true;
            }else{
                var campaign = {
                   id:-1,
                    campaignName: "",
                    leftFaction:leftCard,
                    rightFaction:rightCard,
                    startDate:null,
                    endDate:null
                };
                AdminCampaignService.createCampaign(campaign, function(data){
                    $scope.$apply($scope.currentCampaign = data);
                    $scope.$apply($scope.previewCampaign = data[0]);
                    console.log(data);
                });
                return true;
            }



        };

        var sliderActivationFunction = function () {

            $(".divider").draggable({
                axis: "x",
                containment: "parent",
                scroll: false,
                revert: function(event, ui){
                    $(this).data("ui-draggable").originalPosition = {
                            top: 0,
                            left: 0
                        };
                },
                drag: function () {
                    var p1 = parseInt($("#left-admin-part").width());
                    var p2 = parseInt($("#right-admin-part").width());
                    //var dividerWidth = parseInt()

                    var sum = p1 + p2;
                    var a = parseInt($(this).position().left);
                  //  console.log(a, sum - a, sum);
                    //console.log($scope.leftCard.card, $scope.rightCard.show);
                    if(a < 260){
                        //hide
                        $scope.modalSettings.leftCard.card = false;
                        $scope.modalSettings.leftCard.form = false;
                    }else if(a > 660){
                        //show form
                        $scope.modalSettings.leftCard.card = false;
                        $scope.modalSettings.leftCard.form = true;

                    } else {
                        //show card
                        $scope.modalSettings.leftCard.card = true;
                        $scope.modalSettings.leftCard.form = false;

                    }
                    if((sum - a)< 260){
                        //hide
                        $scope.modalSettings.rightCard.card = false;
                        $scope.modalSettings.rightCard.form = false;
                    }else if((sum - a) > 660){
                        //show form
                        $scope.modalSettings.rightCard.card = false;
                        $scope.modalSettings.rightCard.form = true;

                    } else{
                        //show card
                        $scope.modalSettings.rightCard.card = true;
                        $scope.modalSettings.rightCard.form = false;

                    }

                    $scope.$apply();
                    $scope.modalSettings.rightCard.width = sum - a;
                    $scope.modalSettings.leftCard.width = a;
                    //$("#left-admin-part").css({width:a});
                    //$("#right-admin-part").css({width:sum -a });
                }
            });
        };

    }]).controller('AccountController', function ($scope, $http, $filter, AccountService, ForumManager) {
        $scope.selectedPanel = 0;
        $scope.user = {};
        $scope.posts = {};
        $scope.headers = [];

        AccountService.getUser(function (data) {
            $scope.user = data;
        });



        ForumManager.query(function () {
        });

        ForumManager.resetModal = function () {
            $scope.modalPost = {
                newPost: {
                    title: '',
                    category: '',
                    body: ''
                },
                parentPost: null
            };
        };



        ForumManager.getDataAndHeaders(function (data, headers) {
            $scope.posts = data;
            $scope.headers = headers;
            console.log($filter('filter')($scope.posts.Movies, {userId:"1"}));
            // console.log(data);
            // console.log(headers);
        });
});

