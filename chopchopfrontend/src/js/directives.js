angular.module('ChopChopApp')
    .directive('mainNavigation', function() {
        return {
            restrict: 'E',
            templateUrl: '../../views/nav.html'
        };
    }).directive('loginModal',function(){
        return {
            restrict: 'E',
            scope:false,//use parent scope
            templateUrl: '../../views/loginSignup.html'
        };
    }).directive('voting', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/voting/voting.html',
            controller: 'VotingCtrl'
        };
    }).directive('votingCard', function(){
    return {
        restrict: 'E',
        scope: {
            name: '@',//one way binding
            imagePath: '@',
            description: '@',
            link: '@',
            scoresArray:'=',//two way binding keeps as array not string
            buttonColor: '@',
            disabledFlag: '=',//two way binding
            voteFunction:'&'//method binding
        },
        templateUrl: '../../views/voting/voting-card.html'
    };

    }).directive('votingBarChart', function() {
        return {
            scope: true,
            restrict: 'E',
            templateUrl: 'views/charts/voting-bar-chart.html',
            controller: 'VotingBarChartCtrl'
        };
    }).directive('votingLineChart', function() {
        return {
            scope: true,
            restrict: 'E',
            templateUrl: 'views/charts/voting-line-chart.html',
            controller: 'VotingLineChartCtrl'
        };
    }).directive('shareModal', function() {
        return {
            restrict:'E',
            scope:false,//use parent scope
            templateUrl: '../../views/voting/shareModal.html'
        };
    }).directive('admin', function() {
        return {
            scope: {},
            restrict: 'E',
            templateUrl: 'views/admin/admin.html',
            controller: 'AdminCtrl'
        };
    }).directive('adminModal', function() {
        return {
            restrict:'E',
            scope:false,//use parent scope
            templateUrl: '../../views/admin/adminModal.html'
        };
    }).directive('adminModalForum', function(){
    return {
        restrict: 'E',
        scope: {
            name: '=',//two way binding
            imagePath: '=',
            description: '=',
            link: '=',
            buttonColor: '='
        },
        templateUrl: '../../views/admin/admin-modal-form.html'
    };

    }).directive('forum', function() {
        return {
            restrict: 'E',
            templateUrl: '../../views/forum/forum.html'
        };
    }).directive('forumPost', function() {
    return {
        restrict: 'E',
        scope: {
            headerlimit: '@',
            bodylimit: '@',
            post: '=',
            grow: '&',
            burn: '&',
            reply:'&',
        },
        templateUrl: '../../views/forum/forumPost.html'
    };
    }).directive('forumModal', function() {
        return {
            restrict:'E',
            scope:false,//use parent scope
            templateUrl: '../../views/forum/forumModal.html'
        };
    }).directive('forumResize', function() {
        return function(scope, element, attrs) {
            if (scope.$last){
                $('#forum').height(Math.max($('#forum .layer2').height(), $('#forum').height()));
            }
        };
    })

    .directive('account', function() {
        return {
            restrict: 'E',
            templateUrl: '../../views/account.html'
        };
    // }).directive('about', function() {
    //     return {
    //         restrict: 'E',
    //         templateUrl: '../../views/about.html'
    //     };
    }).directive('accountPanels', function(AccountService){
        return {
            restrict: 'E',
            templateUrl: 'views/account-panels.html',
            scope:false//use parent scope
        };
    }).directive('stopEvent', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                element.bind('click', function (e) {
                    e.stopPropagation();
                });
            }
        };
    });