(function() {
    var app = angular.module('DataManager', []);

    app.service('AccountService', ['$http', '$filter', function($http, $filter) {
        var self = this;

        self.user =  null;
        console.log('loaded account service');

        self.query = function(callback){
            if(!self.user) {

                $http({
                    method: 'GET',
                    url: 'data_mocked/userData.json'
                }).then(function (data) {
                    // console.log('successfully retrieved account data for userId: ' + self.userId,"data", data);
                    //var userData = $filter('filter')(data.data, {userId: self.userId});
                    //console.log(userData[0]);
                    self.user = data.data[1];
                    callback(self.user);
                }, function (error) {
                    console.log('Failure', error);
                });
            }
            else {
                callback(self.user);
            }
        };

        self.getUser = function(callback){
          self.query(callback);
        };
    }])
    .service('ForumManager', ['$http', function ($http) {
            var self = this;
            self.rawData = [];
            self.data = {};
            self.categoryHeaders = [];
            this.query = function (callback) {
                if (Object.keys(self.data).length) {
                    callback(self.data);
                    return;
                }

                $http({
                    method: 'GET',
                    url: 'data_mocked/forumData.json'
                })
                    .then(function (data) {
                        callback(data.data);
                        self.rawData = data.data;
                        self.assembleData(data.data);
                    }, function (error) {
                        console.log('Failure', error);
                    });
            };

            self.assembleData = function (dataArray) {
                dataArray.forEach(function (post) {
                    if (self.data[post.category]) {
                        self.data[post.category].push(post);
                    }
                    else {
                        self.data[post.category] = [post];
                        self.categoryHeaders.push(post.category);
                    }
                });
                //console.log(self.data, self.categoryHeaders);
            };

            self.getDataAndHeaders = function(callback){
                callback(self.data, self.categoryHeaders);
            };

    }]).service('AdminCampaignService', ['$http', function ($http){
        var dummyCampaign = {
            "_id": -1,
            "campaignName": "",
            "leftFaction": {
            "id": 1,
                "name": "",
                "imagePath": "img/",
                "description": "",
                "link": "",
                "color":"green",
                "votes":[]
        },
            "rightFaction": {
            "id": 2,
                "name": "",
                "imagePath": "img/",
                "description": "",
                "link": "",
                "color":"green",
                "votes":[]
        },
            "orderNumber": 0,
            "startDate": null,
            "endDate": null
        };

        //Service functions I will need
        //Admin will use this new service until the campaign service can be updated
        var self = this;
        self.campaigns = null;
        self.nextCampaigns = [];
        self.currentCampaign = {};

        self.query = function (callback) {
            if (self.campaigns) {
                console.log("hit without new data");
                callback(self.campaigns);
                return;
            }

            $http({
                method: 'GET',
                url: 'data_mocked/campaignData.json'
            })
                .then(function (data) {
                    console.log('hit and loaded new data');
                    self.campaigns = data.data;
                    callback(data.data);
                    //self.assembleData(data.data);
                }, function (error) {
                    console.log('Failure', error);
                });
        };

        self.getCurrentCampaign = function(callback){
            //return campaign from list where campaign.order number is 0
            self.query(function(data){
//                var returnObj = {};
                data.map(function (v) {
                    if(v.orderNumber == 0)
                        self.currentCampaign = v;
                });
                console.log("successful map", data, self.currentCampaign  );
                callback(self.currentCampaign);
            });
        };

        self.getCampaigns = function(callback) {
            //returns unmodified and non copied array of campaigns
            //This will allow the front end to directly update the list with small stuff
            //follows MVC pattern
            self.query(callback);
        };

        self.getNextCampaigns = function(callback){
            self.query(function(data){
               //self.nextCampaigns = [];
               //data.map(function (campaign){
               //   if(campaign.order > 0)
               //     self.nextCampaigns.push(campaign);
               //});
               // callback(self.nextCampaigns);
                console.log("pre grep", self.nextCampaigns);
                self.nextCampaigns = $.grep(data, function(campaign){
                    return campaign.orderNumber > 0;
                });
                console.log("survive grep", self.nextCampaigns);
                if(self.nextCampaigns.length > 0) {
                    self.nextCampaigns.sort(function (a, b) {
                        return a.orderNumber > b.orderNumber ? 1 : a.orderNumber == b.orderNumber ? 0 : -1;
                    });
                }
                callback(self.nextCampaigns);
            });
        };

        self.endCampaign = function(callbackCurrent,callbackNext){
            // decrement ALL of the the campaign numbers
            // set currentCampaign equal to the campaign from the list where the campaign.order is 0
            console.log("hit end of campaign");
            self.campaigns.map(function(campaign){
               campaign.orderNumber -= 1;
            });
            self.getCurrentCampaign(callbackCurrent);
            self.getNextCampaigns(callbackNext);
        };

        self.createCampaign = function(campaign, callback) {
            //set campaign order number
            campaign.orderNumber = self.nextCampaigns[self.nextCampaigns.length - 1];

            //This campaign will not have been created
            //grab its left and right factions and create them the db
            //update the objects left and right factions with the new db ones
            //run a create call on campaign
            //append new campaign to

            self.campaigns.push(campaign);//replace with campaign created in callback
            self.getNextCampaigns(callback);
        };

        /**
         This should allow me (the admin control page) to update all atributes includingL
         Start/end dates
         Image paths
         Text
         Swap sides
         **/
        self.updateCampaign = function(campaign){
            //I will have already the data model
            //This will be the specific campaign that has been updates
            //just call the backend update function with this campaign and it's id


        };


    }]).service('CampaignService', ['$http', function($http) {
        var campaigns = [
            {
                campaignName: "The Role of Guns in America",
                factions: [
                    {
                        name: 'The National Rifle Association',
                        imagePath: 'img/guns-guns-guns/national-rifle-association.png',
                        description: "The National Rifle Association is America's foremost defender of Second Amendment rights. Since its inception in 1871, it has also been the premier firearms education organization in the world. Purposes and objectives of the NRA include protecting the right to keep and bear arms, furthering the shooting sports, marksmanship and safety training, and the promotion of hunter safety.",
                        link: 'https://home.nra.org/',
                        votes: []
                    },
                    {
                        name: 'Coalition to End Gun Violence',
                        imagePath: 'img/guns-guns-guns/stop-gun-violence.png',
                        description: 'The Coalition to Stop Gun Violence has lead the way in exposing the dangerous insurrectionist ideology promoted by the NRA and other in the pro-gun movement',
                        link: 'http://csgv.org/',
                        votes: []
                    }
                ],
                dates: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                warScores: [
                    [20, 30, 20, 10, 5, 1, 1],
                    [10, 20, 40, 20, 10, 5, 10]
                ],
                active: true
            }
        ];

        var notifyChanges = function(faction, callback) {
            var success = false;

            if(faction) {
                observers = faction.observers;

                if(observers) {
                    console.log('starting notifying observers');
                    for(var i = 0; i < observers.length; i++) {
                        observer = observers[i];
                        observer(faction);
                    }
                    console.log('all observers notified');
                    success = true;
                }
            }

            if(callback) {
                callback(success);
            } else {
                return success;
            }
        };

        /**
         * Searches for a campaign that matches the given name
         * @param  {String}   searchCampaignName - the name of the campaign to search for
         * @param  {Function} callback           - passes back the found campaign and its index
         */
        var findCampaign = function(searchCampaignName, callback) {
            var foundCampaign = null;
            var foundCampaignIndex = -1;

            for(var i = 0; i < campaigns.length; i++) {
                campaign = campaigns[i];

                if(campaign.campaignName === searchCampaignName) {
                    foundCampaign = campaign;
                    foundCampaignIndex = i;
                    break;
                }
            }

            callback(foundCampaign, foundCampaignIndex);
        };

        /**
         * Searches a campaign for a matching faction
         * @param  {Campaign}   campaign          - the campaign that the faction belongs to
         * @param  {String}   searchFactionName   - the name of the faction
         * @param  {Function} callback            - passes back the found faction and its index
         */
        var findFaction = function(campaign, searchFactionName, callback) {
            var foundFaction = null;
            var foundFactionIndex = -1;

            if(campaign) {
                for(var i = 0; i < campaign.factions.length; i++) {
                    faction = campaign.factions[i];
                    if(faction.name === searchFactionName) {
                        foundFaction = faction;
                        foundFactionIndex = i;
                        break;
                    }
                }
            }

            callback(foundFaction, foundFactionIndex);
        };

        /**
         * Retrieves the first stored faction for the given campaign
         * @param  {String} campaignName - the campaign that the faction belongs to
         * @return {Faction}             - a deep copy of the faction object
         */
        var getLeftFactionData = function(campaignName, callback) {
            var left = {};
            findCampaign(campaignName, function(foundCampaign, foundCampaignIndex) {
                var factions = null;
                if(foundCampaign) {
                    factions = foundCampaign.factions;

                    if(factions) {
                        left = factions[0];
                        left.warScores = foundCampaign.warScores[0];
                    }
                }
            });

            if(callback) {
                callback(left);
            } else {
                return left;
            }
        };

        /**
         * Retrieves the first stored faction for the given campaign
         * @param  {String} campaignName - the campaign that the faction belongs to
         * @return {Faction}             - a deep copy of the faction object
         */
        var getRightFactionData = function(campaignName, callback) {
            var right = {};
            findCampaign(campaignName, function(foundCampaign, foundCampaignIndex) {
                var factions = null;
                if(foundCampaign) {
                    factions = foundCampaign.factions;

                    if(factions && factions.length > 1) {
                        right = factions[1];
                        right.warScores = foundCampaign.warScores[1];
                    }
                }
            });

            if(callback) {
                callback(right);
            } else {
                return right;
            }
        };

        /**
         * Get a faction's image
         * @param  {String}   campaignName - the campaign that the faction belongs to
         * @param  {String}   factionName  - the name of the faction
         * @param  {Function} callback     - passes back the image on the faction
         * @return {String}                - filepath for the campaign image
         */
        var getImagePath = function(campaignName, factionName, callback) {
            var image = null;

            findCampaign(campaignName, function(foundCampaign, foundCampaignIndex) {
                if(foundCampaign) {
                    findFaction(foundCampaign, factionName, function(foundFaction, foundFactionIndex) {
                        if(foundFaction) {
                            image = foundFaction.imagePath;
                        }
                    });
                }
            });

            if(callback) {
                callback(image);
            } else {
                return image;
            }
        };

        /**
         * Set the faction's image
         * @param {String}   campaignName - the campaign that the faction belongs to
         * @param {String}   factionName  - the name of the faction
         * @param {String}   img          - the image
         * @param {Function} callback     - passes back boolean indicating success status
         */
        var setImagePath = function(campaignName, factionName, img, callback) {
            var params = {imagePath: img};
            configureFaction(campaignName, factionName, params, callback);
        };

        /**
         * Parses the campaign data for a score
         * @param  {String}   campaignName - the campaign's name / title
         * @param  {String}   factionName  - the faction's name / title
         * @param  {String}   date         - the date to retrieve from
         * @param  {Function} callback     - passes back the found score (optional)
         */
        var getScore = function(campaignName, factionName, date, callback) {
            var score = null;

            // find the campaign
            findCampaign(campaignName, function(foundCampaign, foundCampaignIndex) {
                // only proceed forward if a matching campaign was found
                if(foundCampaign) {
                    // find the faction
                    findFaction(foundCampaign, factionName, function(foundFaction, foundFactionIndex) {
                        // only proceed forward if a matching faction was found
                        if(foundFaction) {
                            // find the correct score and set it for return
                            var factionScores = foundCampaign.warScores[foundFactionIndex];
                            var scoreIndex = foundCampaign.dates.indexOf(date);

                            score = factionScores[scoreIndex];
                        }
                    });
                }
            });

            if(callback) {
                callback(score);
            } else {
                return score;
            }
        };

        /**
         * Sets a score for a campaign's faction on a certain day
         * @param {String}   campaignName - the campaign's name / title
         * @param {String}   factionName  - the faction's name / title
         * @param {String}   date         - the date to retrieve from
         * @param {Integer}  score        - the value to set the score to
         * @param {Function} callback     - passes back a flag to indicate success (optional)
         */
        var setScore = function(campaignName, factionName, date, score, callback) {
            var scoreWasSet = false;

            // find the campaign
            findCampaign(campaignName, function(foundCampaign, foundCampaignIndex) {
                // only proceed forward if a matching campaign was found
                if(foundCampaign) {
                    // find the faction
                    findFaction(foundCampaign, factionName, function(foundFaction, foundFactionIndex) {
                        // only proceed forward if a matching faction was found
                        if(foundFaction) {

                            // find the correct place to put the score, and set it
                            var factionScores = foundCampaign.warScores[foundFactionIndex];
                            var scoreIndex = foundCampaign.dates.indexOf(date);

                            factionScores[scoreIndex] = score;
                            scoreWasSet = factionScores[scoreIndex] === score;
                        }
                    });
                }
            });

            if(callback) {
                callback();
            }
        };

        /**
         * Vote for somebody
         * @param  {String}   campaignName - the campaign's name / title
         * @param  {String}   factionName  - the faction's name / title
         * @param  {String}   date         - the date to retrieve from
         * @param  {Function} callback     - returns control to the caller (optional)
         */
        function vote(campaignName, factionName, date, callback) {
            getScore(campaignName, factionName, date, function(score) {
                setScore(campaignName, factionName, date, score + 1);
            });

            if(callback) {
                callback();
            }
        }

        /**
         * Marks the specified campaign as inactive
         * @param  {String}   campaignName - the name of the campaign
         * @param  {Function} callback     - returns the success status of ending the campaign (optional)
         * @return {Boolean}               - the success status of ending the campaign
         */
        function endCampaign(campaignName, callback) {
            success = false;
            findCampaign(campaignName, function(foundCampaign, foundCampaignIndex) {
                if(foundCampaign) {
                    foundCampaign.active = false;
                    success = true;
                }
            });

            if(callback) {
                callback(success);
            } else {
                return success;
            }
        }

        /**
         * Marks the specified campaign as active
         * @param  {String}   campaignName - the name of the campaign
         * @param  {Function} callback     - returns the success status of starting (optional)
         * @return {Boolean}               - indicates success status of starting the campaign
         */
        function startCampaign(campaignName, callback) {
            success = false;
            findCampaign(campaignName, function(foundCampaign, foundCampaignIndex) {
                if(foundCampaign) {
                    foundCampaign.active = false;
                    success = true;
                }
            });

            if(callback) {
                callback(success);
            } else {
                return success;
            }
        }

        /**
         * Change individual attributes of a given faction in a given campaign
         * @param  {String}   campaignName - the name of the campaign
         * @param  {String}   factionName  - the name of the faction
         * @param  {Object}   params       - an array with (some) attributes identical to base faction attributes
         * @param  {Function} callback     - returns success as a boolean (optional)
         * @return {Boolean}               - true indicates success, false failure
         */
        function configureFaction(campaignName, factionName, params, callback) {
            success = false;
            findCampaign(campaignName, function(foundCampaign, foundCampaignIndex) {
                // only move forward if a matching campaign is found
                if(foundCampaign) {
                    findFaction(foundCampaign, factionName, function(foundFaction, foundFactionIndex) {
                        // only move forward if a matching faction is found
                        if(foundFaction) {
                            // enumerate all the properties on the passed config object, then start modifying the found campaign
                            var paramsList = Object.keys(params);
                            for(var i = 0; i < paramsList.length; i++) {
                                // only set the value if factions have that property
                                var prop = paramsList[i];
                                if(foundFaction.hasOwnProperty(prop)) {
                                    foundFaction[prop] = params[prop];
                                    // console.log('set faction {' + foundFaction.name + '} property {' + prop + '} to {' + params[prop] + '}');
                                }
                                notifyChanges(foundFaction);
                                success = true;
                            }
                        }
                    });
                }
            });

            if(callback) {
                callback(success);
            } else {
                return success;
            }
        }

        /**
         * Retrieves a faction's votes over a campaign
         * @param  {String}   campaignName - the name of the campaign
         * @param  {String}   factionName  - the name of the faction
         * @param  {Function} callback     - returns vote data to user (optional)
         * @return {Array}                 - an array of numeric values
         */
        function getVotes(campaignName, factionName, callback) {
            var voteCounts = null;

            // find the matching campaign
            findCampaign(campaignName, function(foundCampaign, foundCampaignIndex) {
                // continue if a campaign was found
                if(foundCampaign) {
                    // find the matching faction
                    findFaction(foundCampaign, factionName, function(foundFaction, foundFactionIndex) {
                        // continue if a faction / factionIndex was found
                        if(foundFaction) {
                            // choose the correct warScore
                            voteCounts = foundCampaign.warScores[foundFactionIndex];
                        }
                    });
                }
            });

            if(callback) {
                callback(voteCounts);
            } else {
                return voteCounts;
            }
        }

        /**
         * Retrieves the dates the campaign is run on
         * @param  {String}   campaignName - the campaign's name / title
         * @param  {Function} callback     - returns found dates to the user (optional)
         * @return {Array}                 - a set of dates
         */
        function getDates(campaignName, callback) {
            var foundDates = null;

            // find the matching campaign
            findCampaign(campaignName, function(foundCampaign, foundCampaignIndex) {
                // continue if a campaign was found
                if(foundCampaign) {
                    foundDates = foundCampaign.dates;
                }
            });

            if(callback) {
                callback(foundDates);
            } else {
                return foundDates;
            }
        }

        // expose the following functions on the service
        return {
            getRightFactionData: getRightFactionData,
            getLeftFactionData: getLeftFactionData,
            setImagePath: setImagePath,
            getImagePath: getImagePath,
            getVotes: getVotes,
            getScore: getScore,
            getDates: getDates,
            vote: vote
        };
    }])
    .service('ChartService', ['$http', function() {
        function loadCharts() {
            google.charts.load('current', {packages: ['corechart', 'bar']});
            // google.charts.load('current', {'packages':['corechart','bar']});
        }

        loadCharts();
    }]);

})();

